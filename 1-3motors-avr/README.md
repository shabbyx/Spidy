# Version 1

This is the first version of Spidy.  The design in this version is introduced
[here](http://www.thingiverse.com/make:129549).  The CAD files you could use to 3D print the robot is available in the
`cad/` directory and the source files are available in the `src/` directory.

The accompanying `Makefile` in `src/` builds the code for the particular micro used (Atmega32u4, found on Arduino Micro)
and with the `install` target uploads it.

## Printed Robot

The printed robot has three motors embedded on it, two of which are connected to four-bar linkages driving two legs each.
The third motor lifts the robot on either side to allow the legs to move back and forth but only be in contact with the
ground, and thus move the robot, in one direction.

![Top View](1-3motors-avr/img/spidy-1-top.jpg)

A small breadboard is embedded in the front of the robot for future extensions and is currently unused.  The
microcontroller itself is outside the robot as well as the power source.

![Isometric View](1-3motors-avr/img/spidy-1-isometric.jpg)

The required components for this robot are as follows:

- 3D printed pieces.  You can use the files in the `cad/` directory to print the robot.
- 3 position-controlled servo motors.  I used very cheap servos as the robot is very light.
- Screws and washers.  These are used to create the linkages and connect the motors.  Make sure to put your servo motors
  to home position before connecting everything.
- Glue.  As the 3D printers may have imprecision, you may find the motors not perfectly fitting.  Use glue to hold
  everything together.

You can see this demo in action in the following video.

<a href="http://www.youtube.com/watch?feature=player_embedded&v=TSizcVoh3fM
" target="_blank"><img src="http://img.youtube.com/vi/TSizcVoh3fM/0.jpg" 
alt="Spidy v1 in action" width="480" height="360" border="10" /></a>

## The Code

First of all, get the datasheet of Atmega32u4 and the schematics of Arduino Micro for reference.  For the quickest
tutorial on how position-controlled servo motors work, see the following figure:

![Servo Control](1-3motors-avr/img/servo-tutorial.svg)

Get the datasheet of your servo motor to know the exact numbers, but generally in a 20ms period, having a 1.5ms duty
cycle results in the servo motor to move to its home position, i.e., the middle of its working range.  Play around with
different duty cycles, paying attention to the servo's datasheet, to see how the motor acts.  Once you are comfortable
with servo motors, read on!

### Servo Control

Each servo motor on the robot is controlled by a PWM.  With Atmega32u4, Timer1 is used to generate these PWMs.  Make
sure to read up on the corresponding section in the datasheet of the microcontroller.  In `src/main.c`, the function
`setup_peripherals()` configures Timer1 to generate non-inverted phase and frequency correct PWMs.  This configuration
allows the controller to set the PWM duty cycle any time in the PWM cycle and have it applied on the next cycle, which
prevents extra short or extra long duties due to bad synchronization between the controller and the current timer count.

Timer1 is configured to run at 2MHz, i.e., dividing the clock by 8.  The timer counter counts up and then down.
Therefore, counting up to `N` and back generates a cycle of `N` microseconds.  Henceforth, the values given to Timer1
are expressed in microseconds, which given the above configuration are equal to the actual numbers used by the timer.

Timer1 has the ability to generate 3 separate PWMs, each controlled by a different register; `OCR1A`, `OCR1B` and
`OCR1C`.  We have connected the middle servo motor to `OCR1A`, the left servo motor to `OCR1B` and the right servo motor
to `OCR1C` (the motors are in the back of the robot and the breadboard is in the front).  Initially, these registers are
given a value of 1500, which corresponds to 1500us or 1.5ms, i.e., the home of the servos.

The overall connections of Spidy can be seen in the following image.  The Arduino Micro is assumed to be powered by USB
in this image.

![Connections](1-3motors-avr/img/connections.svg)

### Smooth Movement

The position-controlled servo motors could have jerky movement if told directly where to go to, besides that, you
don't have direct control over the velocity or acceleration of the motor.  The code in `src/servo.c` implements a
function where the servo motor is guided step by step to simulate smooth motion.

In `src/servo.h`, you can find the following structure:

    struct servo_control_data
    {
        /* high level variables */
        int16_t goal;                    /* goal distance from home position */
        int16_t position;                /* current position from home position */
        uint8_t velocity;                /* absolute velocity towards goal */

        /* low level control */
        volatile uint16_t *pwm_register; /* the OCRxy register used to set PWM active time */
    };

The `pwm_register` is one of `OCR1x` corresponding to this motor.  By writing to this address, the controller dictates
the new position of the servo motor.

The high level variables indicate the current position and (simulated) velocity of the motor as well as the goal or
destination of the motor.  The position values are stored relative to home position for the heck of it.  With this
design, the higher level control only tells what the destination of the motor is, and the lower level control manages
its movement.

We want to do smooth control of the servo motor.  This means that given a new goal, the motor needs to start
accelerating up to a maximum speed, continue moving with that speed and then decelerate when it gets near the goal.  In
`src/servo.c`, the algorithm used is explained.  In the same file, `SERVO_ACCELERATION` and `SERVO_MAX_SPEED` show the
acceleration and maximum speed of the movement respectively.  Here, we need basic knowledge of physics.  First, a
review:

- Change in velocity based on acceleration:

        (1) dv = a.dt

  This says that after `dt` time has passed, the velocity changes by `a` times `dt`.  We update the servo motor's
  position step by step, in regular steps, so we can assume `dt` is a constant and just take it as 1.

- Change in position based on velocity and acceleration:

        (2) dx = (1/2).a.dt^2 + v.dt

  This says that after `dt` time has passed, the position changes partly due to the current velocity `v` and partly due
  to the acceleration `a`.

With a combination of these two basic formulas, we can drive the motor smoothly.  At each step, we know the current
position of the motor (because we assigned it in the previous step), and its (simulated) velocity.  Knowing the goal, we
need to decide what acceleration to apply (whether to accelerate, decelerate or continue with this speed).  Having done
that, we calculate `dx` and add it to the current position (which tells the motor where to go next) and update the
simulated velocity by adding `dv` to it.

Knowing when to accelerate and when to decelerate is actually the trickiest part.  First, let's say the motor is very
far from its goal.  In this case, we definitely first accelerate to maximum speed and then start decelerating when
getting close to the goal.  How do we know we are close enough to the goal?  Let's say we plan to decelerate at a
distance such that as the velocity reaches zero, we arrive at the goal.  Assuming velocity `V` and acceleration `A`, the
time `t` it takes to decelerate to zero is given by `t = V/A` (from (1)).  The distance `d` traveled in the meantime is
given by `d = -(1/2).A.t^2 + V.t = (V^2)/2A` (from (2)).  In other words, if the current motor velocity is `V`, then at
distance closer than `(V^2)/2A` we need to decelerate.  If our distance is greater, we can safely accelerate up to the
maximum allowed velocity.

The nice thing about the solution above is that it also works if the goal of the motor is not very far from its current
position.  In such cases, the motor accelerate as much as it can, possibly not reaching the maximum velocity, before it
starts decelerating.  There are a couple of issues though.  Integer calculations have errors.  Therefore, the
deceleration distance is slightly overestimated in the code (in other words, rounded up) so that once decelerating, the
motor doesn't alternate between deceleration and acceleration.  The second issue also has to do with integer
calculations.  Once the motor reaches very close to its goal, small fractions are required to calculate the appropriate
deceleration precisely.  In the code, we simply don't care and once the motor gets close enough to its goal (a value
configured with `SERVO_NEAR_GOAL_THRESHOLD`), the position is set to goal and the motor moves to goal without any
further steps.

### Robot Movements

The robot movements require coordinated movements of the motors and often require a set of different movements performed
cyclically.  With a careful design, these movements can be easily defined without having duplicate code.  In
`src/main.c`, you will find the following data structures.

    struct spidy_calibration
    {
        int16_t home;                    /* where the motor actually centers the leg */
        int16_t range;                   /* how much the motor is actually allowed to move in each direction */
    };

This structure contains calibration data.  `home` tells where is the actual home of the servo motor.  Ideally, this
value would be 0, but if during assembly some link is slightly tilted, this can be used to center it.  `range` shows how
much the motor is allowed to move in either direction before it hits something.

    enum spidy_state
    {
        SPIDY_STILL = 0,                 /* standing still, doing nothing */
        SPIDY_FORWARD = 1,
        SPIDY_BACKWARD = 2,
        SPIDY_TURN_LEFT = 3,
        SPIDY_TURN_RIGHT = 4,
    };

State of Spidy shows which behavior it is currently executing.


    struct spidy_behavior_step
    {
        int direction[3];                /* 1, 0 or -1 for counter-clockwise, none and clockwise of motors middle, left and right */
    };

Each behavior of Spidy, such as going forward, consists of a set of steps.  Each step consists of each motor either not
turning, turning clockwise or turning counter-clockwise.  The `struct spidy_behavior_step` keeps these values for each
motor during a step.

    struct spidy_behavior
    {
        const struct spidy_behavior_step *steps;
        const uint8_t steps_count;
        uint8_t current_step;
    };

Finally, a behavior is a set of steps.  `struct spidy_behavior` holds information on how many and which steps are in a
behavior.  Once a behavior is executed, `current_step` is used to follow the steps through.

`src/main.c` then defines the behaviors.  For example, moving forwards has the following steps:

    static const struct spidy_behavior_step spidy_forward[] = {
        { .direction = { -1, 0, 0}, },
        { .direction = { 0, -1, -1}, },
        { .direction = { 1, 0, 0}, },
        { .direction = { 0, 1, 1}, },
    };

These steps make the robot first lift one side, and then push forwards with the legs contacting the ground while at the
same time moving the legs that are in the air forward in preparation for the next step.  The next two steps are similar,
but now the other side of the robot is lifted in the air.  When applying a step, each motor's goal is set to either
`-range` or `range` (`range` from `spidy_calibration`) only if `direction` is not 0.

    static struct spidy_behavior spidy_behaviors[] = {
        [SPIDY_STILL] = { .steps = spidy_still, .steps_count = sizeof spidy_still / sizeof *spidy_still },
        [SPIDY_FORWARD] = { .steps = spidy_forward, .steps_count = sizeof spidy_forward / sizeof *spidy_forward },
        [SPIDY_BACKWARD] = { .steps = spidy_backward, .steps_count = sizeof spidy_backward / sizeof *spidy_backward },
        [SPIDY_TURN_LEFT] = { .steps = spidy_turn_left, .steps_count = sizeof spidy_turn_left / sizeof *spidy_turn_left },
        [SPIDY_TURN_RIGHT] = { .steps = spidy_turn_right, .steps_count = sizeof spidy_turn_right / sizeof *spidy_turn_right },
    };

The `spidy_behaviors` array lists the possible behaviors and is indexed by `state`.  Using this array, all we need to
know regarding the execution of the behavior is _which_ behavior is being executed.  What's more, there would be no
`if`s or `switch`es on the behavior, which again simplifies the code.

The overall execution of a behavior thus becomes as follows.

1. Wait until all motors have reached their goals
2. Set the new goals for the motors based on current behavior's current step
3. Increment step.  If it was the last step, step would be 0.
4. Go to 1

In `src/main.c`, the behavior of the robot is changed every 15 seconds just for demo.

### Task Scheduling

You might have noticed that we have at least two tasks at hand:

1. Change motor position step by step and frequently
2. Change motor goal possibly every time it is reached

Just for the heck of it, let's also blink a led every second.  As we mentioned before, we also have another task for
demo, and that is to change the behavior every 15 seconds.

To be able to do all these tasks in parallel, the code in `src/main.c` is written using interrupts and is event-driven.
While waiting for interrupt, it puts the microcontroller in sleep mode to save power.  In this version of Spidy, there
is only one interrupt, and that is from a fixed timer.  The general scheduling algorithm is thus as follows.

1. Sleep (wakes up on interrupt)
2. If task A needs doing, do it
3. If task B needs doing, do it
4. If task C needs doing, do it
5. If task D needs doing, do it
6. Go to 1

The trick is for each task to do something quick and move on.  If a task requires waiting for an event, it should be
able to remember this and resume after enough time has passed while letting other tasks continue their execution.

In `src/main.c`, Timer0 is configured to generate an interrupt every 10 milliseconds.  Currently, the smallest task has
a period of 20ms (low level motor control), so the interrupt could have also happened every 20 milliseconds, but what
the heck (I mean, 10ms is easier for calculations in my head).

The timer interrupt essentially increments a set of timers, defined in the following structure.

    struct scheduler_data
    {
        /* counters */
        volatile uint8_t led;
        volatile uint8_t servo_control;
        volatile uint8_t one_second;
        volatile uint8_t change_behavior;
        /* periods */
        uint8_t led_period;
        uint8_t servo_control_period;
        uint8_t change_behavior_period;
    };

In this data structure, every task has a counter and a period.  The counter is incremented by the interrupt service
routine.  `one_second` counts 100 times (100 times 10ms means 1 second) and every time it reaches 100, it resets to zero
and the counter for slower tasks are incremented (in this case, `change_behavior`).

In the main loop of the program, each task counter is checked against its period.  Once it reaches its period, its
period is reduced from the counter (instead of resetting to 0, so in case there was a delay in processing the task, the
next cycle would automatically be shorter), and the task is executed.  In the case of blinking a LED, this would be:

    if (scheduler.led >= scheduler.led_period)
    {
        scheduler.led -= scheduler.led_period;

        PORTC ^= _BV(PORTC7);
    }

Make sure to check the code out for details.
