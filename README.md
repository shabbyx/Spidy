Spidy
=====

This repository contains a set of spider-like robot designs, including both the CAD files and the control codes.  As me
and my wife [Loan](https://github.com/hongloan) experiment and progress through different designs, we'll try to keep
usable versions in different directories with information on how the robot works.

The repository is thus formatted in separate directories, starting with a number that shows the progression towards the
final build.  The `README.md` file in each directory describes the robot and walks through the code, assuming all
previous versions are well-understood by the reader.

While the control algorithms are often generic, they would certainly need to be adapted to your microcontroller.  We
have used an Atmega32u4 (the AVR found on [Arduino Micro](http://arduino.cc/en/Main/ArduinoBoardMicro)) for lower level
control and a [Raspberry Pi B+](http://www.raspberrypi.org/products/model-b-plus/) for higher level control.

All code is released under GNU GPL version 2 or later.

The programs are written under Linux.  To compile the codes for AVR, you would need to install the `avr-libc` package
which installs `binutils-avr` and `gcc-avr` alongside (in Debian-based distributions).  For programming the
microcontroller, you would need `avrdude` (from a package with the same name).

1-3motors-avr
-------------

This is a simple prototype that uses 3 motors to control a hexapod.  This features the lower level control implemented
over the AVR microcontroller and is accompanied by a simple demo loop.  The Raspberry Pi is unused in this version.
